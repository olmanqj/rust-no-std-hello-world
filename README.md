# Rust no-std hello-world

A Rust minimal hello-world using no_std environment! 


## Prerequisits
- The [Rust toolchain](https://www.rust-lang.org/tools/install).
- A [nightly Rust compiler](https://rust-lang.github.io/rustup/installation/index.html) is required.

If not yest installed the nightly version, here the shortcut:
~~~
rustup default nightly
~~~


## Run 
Simply use cargo:
~~~
cargo run
~~~

